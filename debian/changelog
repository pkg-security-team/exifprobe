exifprobe (2.0.1+git20201230.eee65ff-2) unstable; urgency=medium

  * Team upload.

  * d/p/*:
    - Drop 006-fix-makefile.patch, it sets only unneeded gcc flags.
    - Drop 005-add-byteorder.patch, it supports little endianness only.
    - Add patch for applying the correct HOST endianness during the build.
    - Add patch to move prerequisite byteorder.h to the correct target.
    - Add patch to run autopkgtest on s390x successfully.
  * d/salsa-ci.yml: Add variable SALSA_CI_DISABLE_BUILD_PACKAGE_ALL.

 -- Sven Geuer <sge@debian.org>  Wed, 05 Feb 2025 22:57:56 +0100

exifprobe (2.0.1+git20201230.eee65ff-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.0, no changes needed.
  * Update standards version to 4.6.1, no changes needed.

  [ Sven Geuer ]
  * d/watch: Make it functioning again.
  * New upstream snapshot 2.0.1+git20201230.eee65ff
  * d/p/006-fix-makefile.patch: Adapt patch to new snapshot.
  * d/p/*:
    - Drop patches applied upstream.
    - Add patch to fix type of timestamp variable (Closes: #1089549).
    - Add patch fixing formatting errors in exifgrep manpage.
  * d/control: Bump Standards-Version to 4.7.0.
  * d/copyright: Update the Debian Copyright field.

 -- Sven Geuer <sge@debian.org>  Mon, 03 Feb 2025 00:50:27 +0100

exifprobe (2.0.1+git20170416.3c2b769-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields:
      Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Giovani Augusto Ferreira ]
  * Bumped DH level to 13.
  * Bumped standards version to 4.5.1, no changes needed.
  * debian/copyright: updated packaging copyright years.

  [ Samuel Henrique ]
  * Configure git-buildpackage for Debian

 -- Giovani Augusto Ferreira <giovani@debian.org>  Mon, 01 Feb 2021 19:32:35 -0300

exifprobe (2.0.1+git20170416.3c2b769-4) unstable; urgency=medium

  [ Giovani Augusto Ferreira ]
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: Changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field.
  * debian/control:
      - Added the field Rules-Requires-Root: binary-targets.
      - Standards-Version: bumped to 4.4.1 (no changes needed)
  * debian/copyright:
      - Updated years and rights.
  * wrap-and-sort -sa

  [ Samuel Henrique ]
  * Add salsa-ci.yml

 -- Giovani Augusto Ferreira <giovani@debian.org>  Thu, 26 Dec 2019 22:02:51 -0300

exifprobe (2.0.1+git20170416.3c2b769-3) unstable; urgency=medium

  * Bumped DH level to 12
  * Bumped Standards-Version to 4.3.0
  * Added upstream metadata file
  * debian/copyright: updated URI and packaging copyright years.
  * debian/patches/*: renamed all patches to organize better
  * debian/tests/*: created to provide trivial tests.
  * debian/watch: added a mangle rule to handle the '+git' suffix.

 -- Giovani Augusto Ferreira <giovani@debian.org>  Wed, 09 Jan 2019 00:06:01 -0200

exifprobe (2.0.1+git20170416.3c2b769-2) unstable; urgency=medium

  * Team upload.
  * Integrate the NMU. Thanks Adrian.
  * Bump Standards-Version to 4.2.0.
  * Update team maintainer address to Debian Security Tools
    <team+pkg-security@tracker.debian.org>
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org

 -- Raphaël Hertzog <hertzog@debian.org>  Sat, 25 Aug 2018 12:53:53 +0200

exifprobe (2.0.1+git20170416.3c2b769-1.1) unstable; urgency=high

  * Non-maintainer upload.
  * Add upstream fix for segfaults due to missing function prototypes.
    (Closes: #874275)

 -- Adrian Bunk <bunk@debian.org>  Wed, 08 Aug 2018 21:56:31 +0300

exifprobe (2.0.1+git20170416.3c2b769-1) unstable; urgency=medium

  * New upstream release: fixed crashes on corrupt file. (Closes: #809365)
  * Updated my email address.
  * debian/control:
      - Bumped Standards-Version to 4.0.0.
  * debian/copyright: updated the packaging copyright years.
  * debian/patches:
      - fix-issues-found-by-afl.patch: removed, the upstream fixed the
        source code. Thanks.
      - fix-makefile.patch: updated to improve GCC hardening.

 -- Giovani Augusto Ferreira <giovani@debian.org>  Sat, 08 Jul 2017 19:36:23 -0300

exifprobe (2.0.1-11) unstable; urgency=medium

  * Update DH level to 10.
  * debian/compat: updated to 10.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Fri, 16 Dec 2016 22:52:19 -0200

exifprobe (2.0.1-10) unstable; urgency=medium

  * debian/control:
      - Bumped Standards-Version to 3.9.8.
  * debian/patches:
      - Added the '.patch' extension to all patches.
      - fix-manpages.patch: added a typing error correction for exifprobe.1

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Sat, 24 Sep 2016 19:18:13 -0300

exifprobe (2.0.1-9) unstable; urgency=medium

  * debian/control: Changed from cgit to git in Vcs-Browser field.
  * debian/patches/fix_spelling_error:
       - Added a typing error correction for misc.c.
  * debian/watch: bumped to version 4.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Sun, 20 Mar 2016 22:30:48 -0300

exifprobe (2.0.1-8) unstable; urgency=medium

  * New upload to try rebuild and fix powerpc.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Sun, 06 Mar 2016 11:47:47 -0300

exifprobe (2.0.1-7) unstable; urgency=medium

  * New co-maintainer. Thanks Eriberto Mota for great work in the last uploads.
  * debian/control:
      - Bumped Standards-Version to 3.9.7.
      - Updated Vcs-* fields.
  * debian/copyright:
      - Added my name at debian/* block.
      - Dropped dot-zero from GPL license short name.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Sat, 20 Feb 2016 23:42:09 -0200

exifprobe (2.0.1-6) unstable; urgency=medium

  * Upload to unstable.
  * debian/control: added the Homepage field.
  * debian/patches/fix-issues-found-by-afl: added to fix some issues detected
      by AFL. (Closes: #779525, #779527)
  * debian/watch: pointing to new upstream site.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 28 May 2015 18:58:35 -0300

exifprobe (2.0.1-5) experimental; urgency=medium

  * debian/copyright: updated the packaging copyright years.
  * debian/patches/fix-embed-build-time: created to fix an issue while building.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 26 Feb 2015 13:24:54 -0300

exifprobe (2.0.1-4) experimental; urgency=medium

  * debian/control:
      - Improved the long description.
      - Removed the erroneous homepage. Currently, the program
        hasn't a homepage.
  * debian/copyright: removed the erroneous homepage from
      Source field. Currently, the program hasn't a homepage.
  * debian/watch: updated to use a fake site.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 29 Oct 2014 15:36:45 -0200

exifprobe (2.0.1-3) unstable; urgency=medium

  * New maintainer and co-maintainer. Thanks a lot to Martin Albisetti,
      the initial maintainer, for your nice work over this package.
      (Closes: #738911)
  * Migrations:
      - Copyright to 1.0 format.
      - Debian format from 1.0 to 3.0.
      - debian/rules to new (reduced) format.
  * debian/control:
      - Added the Vcs-* fields to control the Debian packaging.
      - Added 'xutils-dev' to Build-Depends field.
      - Added the Homepage field.
      - Bumped Standards-Version to 3.9.6.
      - Removed the reference to upstream homepage from long description.
      - Removed the duplicated 'most' word. Thanks to Aputsiaq Janussen
        <aj@isit.gl>. (Closes: #641179)
      - Updated the short and long description.
  * debian/dirs: useless. Removed.
  * debian/docs:
      - Moved CHANGES_2.0 to new debian/exifprobe.changelog file.
      - Removed the LICENSE.EXIFPROBE file.
  * debian/patches/:
      - Added the 'add_byteorder' and 'Makefile' patches to replace the
        direct changes made in upstream code in past.
      - Added the 'fix_manpages' and 'fix_spelling_error' patches.
      - Added the 'fix-exifgrep' patch. Thanks a lot to Daniel Friesel
        <derf@chaosdorf.de>. (Closes: #597123)
  * debian/rules: set a special target to install the upstream changelog.
      (Closes: #670994)
  * debian/watch: added. (Closes: #462040)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 27 Oct 2014 09:29:00 -0200

exifprobe (2.0.1-2) unstable; urgency=medium

  * QA upload.
  * Updated the DH from 5 to 9.
  * debian/control:
      - Set Debian QA Group as maintainer (see #738911).
      - Updated the Standards-Version from 3.7.2 to 3.9.5.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 23 Aug 2014 23:49:45 -0300

exifprobe (2.0.1-1) unstable; urgency=low

  * Initial release (Closes: #428748)
  * Fixed Makefile and emailed changes upstream

 -- Martin Albisetti <argentina@gmail.com>  Wed, 13 Jun 2007 16:31:10 +0100
